class CreatePromotionsCarts < ActiveRecord::Migration[5.2]
  def change
    create_table :promotions_carts do |t|
      t.references :promotions, foreign_key: true
      t.references :cart, foreign_key: true

      t.timestamps
    end
  end
end
