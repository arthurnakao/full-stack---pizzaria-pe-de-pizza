Rails.application.routes.draw do
  resources :promotions
  resources :carts
  resources :products

  get 'sessions/new'
  get 'sessions/index'
  resources :users

  root 'sessions#index'       #Alterar Para página certa

  

  get    'sign_in'   => 'sessions#new'
  post   'sign_in'   => 'sessions#create'
  delete 'sign_out'  => 'sessions#destroy'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
