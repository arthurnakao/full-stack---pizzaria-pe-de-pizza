class Product < ApplicationRecord
    has_many :carts, through: :product_cart
end
