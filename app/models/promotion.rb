class Promotion < ApplicationRecord
    has_many :carts, through: :promotions_cart
end
