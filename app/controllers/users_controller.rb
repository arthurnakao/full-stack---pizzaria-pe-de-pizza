class UsersController < ApplicationController
  before_action :authorize, except: [:new, :create]

  def new
    if logado?
      redirect_to sessions_index_path
    end
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if(@user.save)
      redirect_to @user, notice: "Usuário criado com sucesso!"
    else
      render action: :new
    end
  end

  def show
    @user=User.find(params[:id])
  end

  def index
    @users=User.all
  end

  def index_admin

  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = current_user
    if @user.update(user_params)
      redirect_to @user
    else
      render 'edit'
    end
  end

  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :address, :phone, :payment_method, :admin)
  end
end
